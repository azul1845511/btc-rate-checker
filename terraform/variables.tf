# AWS Region
variable "aws_region" {
  description = "Region in which AWS Resources to be created"
  type        = string
  default     = "ap-south-1"
}



variable "vpc_name" {
  description = "Name of the VPC"
  type        = string
}



variable "cluster_name" {
  description = "Name of the EKS Cluster"
  type        = string
}




variable "vpc_cidr" {
  description = "CIDR Block Used for EKS Cluster VPC"
  type        = string

}

variable "vpc_private_subnets" {
  description = "List of Private Subnets for EKS Cluster VPC"
  type        = list(string)
}


variable "vpc_public_subnets" {
  description = "List of Public Subnets for EKS Cluster VPC"
  type        = list(string)
}

variable "vpc_enable_nat_gateway" {
  description = "Enable NAT Gateway or Not"
  type        = bool
  default     = false
}

variable "vpc_single_nat_gateway" {
  description = "Enable Single NAT Gateway or Not"
  type        = bool
  default     = false
}

variable "vpc_enable_dns_hostnames" {
  description = "Enable NAT Gateway or Not"
  type        = bool
  default     = false
}

variable "vpc_database_subnets" {
  description = "Subnets for RDS"
  type        = list(string)
}



# variable "argocd_enabled" {
#   description = "Enable Argo CD installation"
#   type        = bool
#   default     = false
# }
# # terraform apply -var="argocd_enabled=true"

variable "argocd_apps_enabled" {
  description = "Enable Argo CD installation"
  type        = bool
  default     = false
}
# terraform apply -var="argocd_apps_enabled=true"