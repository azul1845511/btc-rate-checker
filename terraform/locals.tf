locals {
  common_tags = {
    Project    = "Azul"
    Deployment = "gitlab-ci-pipeline"
  }
}

resource "random_string" "random" {
  length  = 8
  special = false
  upper   = false
}

locals {
  eks_vpc_name     = "${var.vpc_name}-${random_string.random.result}"
  eks_cluster_name = "${var.cluster_name}-${random_string.random.result}"
}

