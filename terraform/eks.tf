module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 20.0"

  cluster_name    = local.eks_cluster_name
  cluster_version = "1.29"

  cluster_endpoint_public_access = true

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
  }

  vpc_id                   = module.vpc.vpc_id
  subnet_ids               = module.vpc.private_subnets
  control_plane_subnet_ids = module.vpc.public_subnets

  cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  # EKS Managed Node Group(s)
  eks_managed_node_group_defaults = {
    instance_types = ["t3a.small"]
  }

  eks_managed_node_groups = {
    azul = {
      min_size     = 5
      max_size     = 10
      desired_size = 5

      instance_types = ["t3a.small"]
      capacity_type  = "ON_DEMAND"
    }

    remote_access = {
      ec2_ssh_key               = "amey-proton"
      source_security_group_ids = [aws_security_group.remote_access.id]
    }
  }

  # Cluster access entry
  # To add the current caller identity as an administrator
  enable_cluster_creator_admin_permissions = true

  access_entries = {
    # One access entry with a policy associated
    amey = {
      kubernetes_groups = []
      principal_arn     = "arn:aws:iam::698572942369:root"

      # I have just added this as my cli & console users are diffrent 
      policy_associations = {
        amey = {
          policy_arn = "arn:aws:eks::aws:cluster-access-policy/AmazonEKSClusterAdminPolicy"
          access_scope = {
            type = "cluster"
          }
        }
      }
    }
  }

  tags = {
    Environment = "dev"
    Terraform   = "true"
  }

  depends_on = [module.vpc]
}



module "load_balancer_controller" {
  source = "git::https://github.com/DNXLabs/terraform-aws-eks-lb-controller.git"

  cluster_identity_oidc_issuer     = module.eks.cluster_oidc_issuer_url
  cluster_identity_oidc_issuer_arn = module.eks.oidc_provider_arn
  cluster_name                     = local.eks_cluster_name

  depends_on = [module.eks]
}

# Discover the Cluster Token for Auth
data "aws_eks_cluster_auth" "cluster_auth" {
  depends_on = [module.eks.cluster_id]
  name       = module.eks.cluster_name
}




resource "aws_iam_policy" "s3_full_access" {
  name   = "eks-s3-full-access"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": "*"
    }
  ]
}
EOF

  depends_on = [module.eks]

}

resource "aws_iam_role" "eks_s3_role" {
  name = "eks-s3-access-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Federated = module.eks.oidc_provider_arn
        },
        Action = "sts:AssumeRoleWithWebIdentity",
        Condition = {
          StringEquals = {
            "${replace(module.eks.cluster_oidc_issuer_url, "https://", "")}:sub" : "system:serviceaccount:default:s3-runner"
          }
        }
      },
    ]
  })

  depends_on = [module.eks]
}


resource "aws_iam_role_policy_attachment" "s3_full_access_attachment" {
  role       = aws_iam_role.eks_s3_role.name
  policy_arn = aws_iam_policy.s3_full_access.arn

  depends_on = [module.eks]
}



resource "kubernetes_service_account_v1" "s3-access-sa" {
  depends_on = [aws_iam_role_policy_attachment.s3_full_access_attachment, module.eks]
  metadata {
    name = "s3-runner"
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.eks_s3_role.arn
    }
  }
}



resource "aws_security_group" "remote_access" {
  name_prefix = "remote-access"
  description = "Allow remote SSH access"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "SSH access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}