count=0

if terraform state list module.eks >/dev/null 2>&1; then
  echo "terraform state list module.eks exists in the state."
  count=$((count + 1))
  echo $count
else
  echo "terraform state list module.eks does not exist in the state."
  count=$((count - 1))
  echo $count
fi


if terraform state list kubernetes_namespace.argocd >/dev/null 2>&1; then
  echo "terraform state list kubernetes_namespace.argocd exists in the state."
  count=$((count + 1))
  echo $count
else
  echo "terraform state list kubernetes_namespace.argocd does not exist in the state."
  count=$((count - 1))
  echo $count
fi

if terraform state list helm_release.argocd >/dev/null 2>&1; then
  echo "terraform state list helm_release.argocd exists in the state."
  count=$((count + 1))
  echo $count
else
  echo "terraform state list helm_release.argocd does not exist in the state."
  count=$((count - 1))
  echo $count
fi

echo "Total Count : $count"

if [ $count -eq 3 ]; then
  echo "false" > ../run-cluster-creation.flag
else
  echo "true" > ../run-cluster-creation.flag
fi