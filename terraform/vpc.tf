module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.5.3"

  name = local.eks_vpc_name
  cidr = var.vpc_cidr
  azs  = data.aws_availability_zones.available.names


  private_subnets = var.vpc_private_subnets
  public_subnets  = var.vpc_public_subnets

  enable_nat_gateway = var.vpc_enable_nat_gateway
  single_nat_gateway = var.vpc_single_nat_gateway

  enable_dns_hostnames = var.vpc_enable_dns_hostnames

  map_public_ip_on_launch = true

  tags = merge(
    {
      "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    },
    local.common_tags
  )

  public_subnet_tags = merge(
    {
      "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
      "kubernetes.io/role/elb"                    = "1"
    },
    local.common_tags
  )

  private_subnet_tags = merge(
    {
      "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
      "kubernetes.io/role/internal-elb"           = "1"
    },
    local.common_tags
  )


}