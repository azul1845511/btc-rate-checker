# Project Overview

This application provides real-time Bitcoin (BTC) conversion rates specifically tailored for Czechia. It retrieves the necessary dataset from [Coinbase API](https://api.coinbase.com/v2/exchange-rates?currency=BTC) and uploads it to an Amazon S3 Bucket. Subsequently, the application extracts and displays Czechia-related data on a dedicated webpage. 

The architecture of this application is containerized and consists of two distinct containers. The first container hosts the frontend, while the second container operates as a cron job responsible for data retrieval and S3 upload tasks. The cron job container runs every hour to ensure the data is regularly updated.

#### Technology Stack

- **Infrastructure Deployment**: Employing Terraform, the application is deployed on an Amazon EKS (Elastic Kubernetes Service) Cluster.
- **Continuous Delivery**: ArgoCD is utilized for streamlined Continuous Delivery (CD) operations.

#### Components within Amazon EKS Cluster

1. **ALB Ingress**: Facilitates access for both the application and ArgoCD's User Interface.
2. **Deployment (Frontend Container)**: Hosts the frontend component of the application.
3. **CronJobs (Data Fetching and S3 Upload)**: Scheduled to run every hour, responsible for fetching datasets and uploading them to Amazon S3.

----------

**Kubernetes Manifests Repository:** [btc-rate-checker-k8s-manifests](https://gitlab.com/azul1845511/btc-rate-checker-k8s-manifests.git)



# GitLab CI Pipeline

The GitLab CI pipeline automates the deployment process for this project. Below are the stages involved:

#### 1. **Build Stage**
   - Builds the Docker image for the Flask application and the Kubernetes Cron Job.
   - Pushes the built images to Amazon ECR.

#### 2. **Image Scanning Stage**
   - Utilizes Trivy to scan the Docker images for vulnerabilities.

#### 3. **Update Image Tags Stage**
   - Updates the image tags in the Kubernetes manifests to the latest versions.

#### 4. **EKS Cluster Stage**
   - Checks pre-requisites and creates an Amazon EKS cluster using Terraform.
   - Deploys ArgoCD Application to manage deployments on the EKS cluster.

#### 5. **Destroy Infrastructure Stage**
   - Allows manual destruction of the EKS cluster if required.

#### 6. **Rollback Stage**
   - Enables manual rollback of the Kubernetes deployment to the previous versions if necessary.


# Prerequisites

Before setting up the GitLab CI pipeline for your application, ensure you have the following prerequisites in place:

#### 1. **GitLab Runner**
   - GitLab Runners are essential for executing CI/CD pipelines. This project exclusively utilizes Docker executors, so ensure you configure GitLab Runners accordingly.

#### 2. **Environmental Variables**
   - **AWS_ACCESS_KEY_ID**: 
     - Obtain your AWS Access Key ID from the AWS IAM (Identity and Access Management) Console. This key is required for authentication with AWS services.
   
   - **AWS_SECRET_ACCESS_KEY**: 
     - Obtain your AWS Secret Access Key from the AWS IAM Console. This key is used alongside the Access Key ID for authentication purposes.
   
   - **AWS_DEFAULT_REGION**: 
     - Specify your preferred default AWS Region where your resources will be provisioned.
   
   - **CREDENTIALS**: 
     - This variable is crucial for Kaniko to authenticate with Amazon ECR (Elastic Container Registry) and push Docker images. Encode your AWS credentials (Access Key ID and Secret Access Key) in base64 format as shown below:
   
     ```bash
     echo -n "[default]\naws_access_key_id = XXXXXXXXXXXXX\naws_secret_access_key = YYYYYYYYYYYYYYYYYYYYY" | base64
     ```
   
     Replace `XXXXXXXXXXXXX` with your AWS Access Key ID and `YYYYYYYYYYYYYYYYYYYY` with your AWS Secret Access Key. Use the output obtained after base64 encoding as the value for the `CREDENTIALS` variable.
   
   - **GITLAB_TOKEN_K8**:
     - Create an Access Token for the repository containing your Kubernetes manifests. This token will be used in the CI process to modify image tags.
   
   - **AWS_ECR_ID**:
     - Set the ECR (Elastic Container Registry) ID corresponding to your AWS account. For example, if your ECR URI is `11111111111.dkr.ecr.ap-south-1.amazonaws.com/azul-demo`, set `AWS_ECR_ID` to `11111111111.dkr.ecr.ap-south-1.amazonaws.com/azul-demo`.
   
#### 3. **AWS CLI**
   - Install and configure the AWS CLI (Command Line Interface) on your local machine. This tool will be used for interacting with AWS services and managing your resources.


# Pipeline Execution
After ensuring that all prerequisites are set up, you can proceed to trigger the pipeline. Please note that during the initial run, the pipeline may take some time as the entire EKS Cluster needs to be created.



# Implementation Challenges

Throughout the implementation of the GitLab CI pipeline for this project, I faced several significant challenges. Here are the key issues I encountered, along with the solutions I devised to address them:

#### Building and Pushing Docker Images Securely

**Challenge:** My initial strategy was to exclusively use Docker executors, prioritizing security. However, I quickly encountered difficulties in building and pushing Docker images within a Docker container (executor) due to security risks associated with traditional methods like mounting `docker.sock` and Docker-in-Docker (DIND).

**Solution:** After a thorough investigation, I discovered **Kaniko**, a tool designed for building Docker images from a Dockerfile without the need for `docker.sock` or DIND, thus sidestepping the security concerns. Integrating Kaniko into the build stages allowed me to securely push images to AWS Elastic Container Registry (ECR) and package images into tar files for artifact upload, aligning with my security standards while meeting the project's needs.

#### Image Scanning Without Docker Daemon

**Challenge:** Similar to the build stage, the image scanning phase also faced security constraints with methods that required Docker daemon access. The challenge was to find a way to scan Docker images securely and effectively without these risks.

**Solution:** By employing **Trivy**, an open-source vulnerability scanner capable of scanning images stored as tar files, I solved this challenge. This was made possible by Kaniko's ability to convert Docker images into tar files during the build stage, thus allowing Trivy to scan these images without compromising security, ensuring the project's images were free from critical vulnerabilities.

#### Coordinating EKS Cluster and ArgoCD Deployments

**Challenge:** Deploying to Amazon EKS (Elastic Kubernetes Service) clusters introduced a sequencing challenge, particularly with ArgoCD application deployments. The process required the EKS cluster to be in place before proceeding with ArgoCD deployments, necessitating a nuanced approach to `terraform apply` executions.

**Solution:** To address this, I implemented a **pre-deploy check stage** in the pipeline. This stage ensures the EKS cluster's existence and readiness prior to deploying ArgoCD applications, facilitating a controlled deployment process. This strategic check prevented deployment failures and ensured a seamless integration within the CI/CD workflow, illustrating the importance of thoughtful pipeline sequencing.



# Steps to Follow When Destroying the Infrastructure

Before manually triggering the job to destroy the cluster, ensure you complete the following preparatory steps to avoid complications, particularly with VPC destruction due to active Application Load Balancers (ALBs).

#### Step 1: Trigger the `destroy-argocd` Job

Initiate the `destroy-argocd` job as your first action. This job handles the safe removal of ArgoCD components from the cluster.

#### Step 2: Manually Delete Ingress Resources

Before proceeding with the cluster destruction, manually delete the ingress resources to ensure that the associated ALBs are correctly deregistered. This step is crucial to avoid issues with VPC destruction due to lingering ALB instances.

Run the following commands:

```bash
# Check for existing ingress in the default namespace and note the details
kubectl get ingress -n default

# The output might look like this:
# NAME           CLASS    HOSTS   ADDRESS                                                              PORTS   AGE
# azul-ingress   <none>   *       k8s-azulproject-23241cf120-1186578791.ap-south-1.elb.amazonaws.com   80      114m

# Delete the ingress in the default namespace
kubectl delete ingress azul-ingress -n default

# Check for existing ingress in the argocd namespace
kubectl get ingress -n argocd

# The output might show something like:
# NAME                    CLASS    HOSTS   ADDRESS                                                              PORTS   AGE
# argocd-server-ingress   <none>   *       k8s-azulproject-23241cf120-1186578791.ap-south-1.elb.amazonaws.com   80      116m

# Delete the ingress in the argocd namespace
kubectl delete ingress argocd-server-ingress -n argocd
```

Both of these ingress resources utilize the same ALB. Deleting them ensures that the ALB is deactivated and will not interfere with the VPC destruction process.

#### Step 3: Trigger the `destroy-cluster` Job

Once the ingress resources are deleted, and the ALBs are confirmed to be deregistered, proceed to trigger the `destroy-cluster` job. This job will safely tear down the entire cluster, including the VPC and other associated resources, without getting stuck due to active ALBs.

Following these steps in order will ensure a smooth and error-free destruction of the infrastructure, adhering to best practices for resource management and cleanup.

