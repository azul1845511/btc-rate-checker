import requests
import boto3
from datetime import datetime
import json
import os

# Fetch data from API
response = requests.get("https://api.coinbase.com/v2/exchange-rates?currency=BTC")
data = response.json()

# Generate timestamped filename
filename = f"dataset_{datetime.now().strftime('%Y%m%d_%H%M%S')}.json"

# Save data to a file
with open(filename, 'w') as f:
    json.dump(data, f)

# Upload to S3
s3 = boto3.client('s3')
bucket_name = 'terraform-backend-am3y-2'

s3.upload_file(filename, bucket_name, f"temp/{filename}")
os.remove(filename)
